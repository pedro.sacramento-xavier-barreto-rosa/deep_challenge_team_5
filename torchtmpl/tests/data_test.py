import numpy as np

from ..data import *

np.random.seed(0)


def test_geolife_dataset(root_path):
    ds = GeoLifeDataset(root_path)

    print(f"categories: {ds.categories}")

    idxs = [np.random.randint(0, len(ds)) for _ in range(1)]

    for idx in idxs:
        print(f"item {idx} in dataset: {ds[idx]}")


if __name__ == "__main__":
    test_geolife_dataset("/mounts/Datasets4/GeoLifeCLEF2022")
