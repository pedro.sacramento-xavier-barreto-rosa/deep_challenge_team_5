import operator
from functools import reduce

import timm
import torch
import torch.nn as nn

from torchtmpl.models.handlers import BaseTorchHandler


class VanillaCNN(BaseTorchHandler):
    def _create_model(self, cfg, input_size, num_classes):
        layers = []
        cin = input_size[0]
        cout = 16
        for i in range(cfg["num_layers"]):
            layers.extend(self.conv_relu_bn(cin, cout))
            layers.extend(self.conv_relu_bn(cout, cout))
            layers.extend(self.conv_down(cout, 2 * cout))
            cin = 2 * cout
            cout = 2 * cout
        conv_model = nn.Sequential(*layers)

        # Compute the output size of the convolutional part
        probing_tensor = torch.zeros((1,) + input_size)
        out_cnn = conv_model(probing_tensor)  # B, K, H, W
        num_features = reduce(operator.mul, out_cnn.shape[1:], 1)
        out_layers = [nn.Flatten(start_dim=1), nn.Linear(num_features, num_classes)]
        return nn.Sequential(conv_model, *out_layers)

    @staticmethod
    def conv_relu_bn(cin, cout):
        return [
            nn.Conv2d(cin, cout, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.BatchNorm2d(cout),
        ]

    @staticmethod
    def conv_down(cin, cout):
        return [
            nn.Conv2d(cin, cout, kernel_size=2, stride=2, padding=0),
            nn.ReLU(),
            nn.BatchNorm2d(cout),
        ]


class TimmCNN(BaseTorchHandler):
    def _create_model(self, cfg, input_size, num_classes):
        model_name = cfg["model_name"]
        kwargs = cfg["kwargs"]

        cin = input_size[0]

        model = timm.create_model(
            model_name, num_classes=num_classes, in_chans=cin, **kwargs
        )
        return model


class ResNet(BaseTorchHandler):
    def _create_model(self, cfg, input_size, num_classes):
        kwargs = cfg["kwargs"]
        cin = input_size[0]

        model = timm.create_model(
            "resnet" + str(cfg["depth"]),
            num_classes=num_classes,
            in_chans=cin,
            **kwargs
        )
        return model
