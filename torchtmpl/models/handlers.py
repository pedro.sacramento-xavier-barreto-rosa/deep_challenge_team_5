from abc import abstractmethod

import torch
import torchinfo.torchinfo as torchinfo
from tqdm import tqdm

from .. import data, optim, utils


class BaseHandler:
    @abstractmethod
    def _create_model(self):
        pass

    @abstractmethod
    def train(self):
        pass

    @abstractmethod
    def test(self):
        pass


class BaseTorchHandler(BaseHandler):
    use_cuda = torch.cuda.is_available()

    def __init__(self, cfg, input_size, num_classes):
        self.device = torch.device("cuda") if self.use_cuda else torch.device("cpu")

        self.model = self._create_model(cfg, input_size, num_classes)
        self.model.to(self.device)

        self.f_loss = optim.get_loss(cfg["loss"])

        optim_config = cfg["optim"]
        self.optimizer = optim.get_optimizer(optim_config, self.model.parameters())

        self.input_size = input_size
        self.num_classes = num_classes

    def to(self, device):
        self.model.to(device)

    def train(self, loader, dynamic_display=True):
        """
        Train a model for one epoch, iterating over the loader
        using the f_loss to compute the loss and the optimizer
        to update the parameters of the model.
        Arguments :
        model     -- A torch.nn.Module object
        loader    -- A torch.utils.data.DataLoader
        Returns :
        The averaged train metrics computed over a sliding window
        """

        # We enter train mode.
        # This is important for layers such as dropout, batchnorm, ...
        self.model.train()

        total_loss = 0
        num_samples = 0
        for i, (inputs, targets) in (pbar := tqdm(enumerate(loader))):
            inputs, targets = inputs.to(self.device), targets.to(self.device)

            # Compute the forward propagation
            outputs = self.model(inputs)

            loss = self.f_loss(outputs, targets)

            # Backward and optimize
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            # Update the metrics
            # We here consider the loss is batch normalized
            total_loss += inputs.shape[0] * loss.item()
            num_samples += inputs.shape[0]
            pbar.set_description(f"Train loss : {total_loss/num_samples:.2f}")

        return total_loss / num_samples

    def test(self, loader):
        """
        Test a model over the loader
        using the f_loss as metrics
        Arguments :
        model     -- A torch.nn.Module object
        loader    -- A torch.utils.data.DataLoader
        f_loss    -- The loss function, i.e. a loss Module
        Returns :
        """

        # We enter eval mode.
        # This is important for layers such as dropout, batchnorm, ...
        self.model.eval()
        loss_fn = lambda x, y: torch.tensor(
            [self.f_loss(x, y), utils.top30_error_rate(x, y)]
        )

        total_loss = 0
        num_samples = 0
        for inputs, targets in loader:
            inputs, targets = inputs.to(self.device), targets.to(self.device)

            # Compute the forward propagation
            outputs = self.model(inputs)

            loss = loss_fn(outputs, targets)

            # Update the metrics
            # We here consider the loss is batch normalized
            total_loss += inputs.shape[0] * loss
            num_samples += inputs.shape[0]

        return (total_loss / num_samples).tolist()

    def load(self, model_path):
        self.model.load_state_dict(torch.load(model_path))

        # Move model to device and set to evaluation mode
        self.model.to(self.device)
        self.model.eval()

    @staticmethod
    def get_dataloaders(cfg):
        return data.get_dataloaders(cfg, BaseTorchHandler.use_cuda)

    @staticmethod
    def get_dataloaders_submission(submission_config):
        test_loader, input_size, num_classes = data.get_dataloaders_submission(
            submission_config, BaseTorchHandler.use_cuda
        )
        test_loader = tqdm(test_loader, desc="Processing batches", unit="batch")
        return test_loader, input_size, num_classes

    def update_summary(self, summary_info):
        summary_info["model_info"] = torchinfo.summary(
            self.model, input_size=summary_info["input_size"], verbose=0
        )
        summary_info["f_loss"] = self.f_loss
        return summary_info
