# coding: utf-8

# Standard imports
import operator
from functools import reduce

# External imports
import torch.nn as nn


class Linear:
    def _create_model(self, cfg, input_size, num_classes):
        """
        cfg: a dictionnary with possibly some parameters
        input_size: (C, H, W) input size tensor
        num_classes: int
        """
        layers = [
            nn.Flatten(start_dim=1),
            nn.Linear(reduce(operator.mul, input_size, 1), num_classes),
        ]
        return nn.Sequential(*layers)


class Linear:
    def _create_model(self, cfg, input_size, num_classes):
        """
        cfg: a dictionnary with possibly some parameters
        input_size: (C, H, W) input size tensor
        num_classes: int
        """
        layers = [
            nn.Flatten(start_dim=1),
            nn.Linear(reduce(operator.mul, input_size, 1), num_classes),
            nn.ReLU(inplace=True),
            nn.Softmax(dim=1),
        ]
        return nn.Sequential(*layers)
