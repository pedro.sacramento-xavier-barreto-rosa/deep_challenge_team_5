# coding: utf-8

# Standard imports
import datetime
import logging
import os
import pathlib
import sys
from pprint import pformat, pprint

import pandas as pd
import torch
import torch.nn.functional as F
import torch.utils.data

# External imports
import yaml

import wandb

# Local imports
from . import models, utils


def train(config):
    if "wandb" in config["logging"]:
        wandb_config = config["logging"]["wandb"]
        wandb.init(
            project=wandb_config["project"],
            entity=wandb_config["entity"],
            tags=wandb_config["entity"],
        )
        wandb_log = wandb.log
        wandb_log(config)
        logging.info(f"Will be recording in wandb run name : {wandb.run.name}")
    else:
        wandb_log = None

    # Get the model name
    model_config = config["model"]
    handler_class = models.get_handler_name(model_config)

    # Build the dataloaders
    logging.info("= Building the dataloaders")
    data_config = config["data"]
    train_loader, valid_loader, input_size, num_classes = handler_class.get_dataloaders(
        data_config
    )

    # Build model
    logging.info("= Model")
    handler = handler_class(model_config, input_size, num_classes)

    # Build the callbacks
    logging_config = config["logging"]
    # Let us use as base logname the class name of the modek
    logname = model_config["class"]
    logdir = utils.generate_unique_logpath(logging_config["logdir"], logname)
    if not os.path.isdir(logdir):
        os.makedirs(logdir)
    logging.info(f"Will be logging into {logdir}")

    # Copy the config file into the logdir
    logdir = pathlib.Path(logdir)
    with open(logdir / "config.yaml", "w") as file:
        yaml.dump(config, file)

    # Make a summary script of the experiment
    summary_info = {
        "command": " ".join(sys.argv),
        "logdir": logdir,
        "config": config,
        "input_size": next(iter(train_loader))[0].shape,
        "dataset_train": train_loader.dataset.dataset,
        "dataset_validation": valid_loader.dataset.dataset,
        "wandb": (f"run name : {wandb.run.name}\n\n" if wandb_log is not None else ""),
    }

    summary_info = handler.update_summary(summary_info)
    pprint(summary_info)
    summary_text = pformat(summary_info)

    with open(logdir / "summary.txt", "w") as f:
        f.write(summary_text)
    logging.info(summary_text)
    if wandb_log is not None:
        wandb.log({"summary": summary_text})

    # Define the early stopping callback
    model_checkpoint = utils.ModelCheckpoint(
        handler.model, str(logdir / "best_model.pt"), min_is_best=True
    )

    for e in range(config["nepochs"]):
        # Train 1 epoch
        train_loss = handler.train(train_loader)

        # Test
        loss_error, top30_error_rate = handler.test(valid_loader)

        updated = model_checkpoint.update(loss_error)
        logging.info(
            "[%d/%d] Cross Entropy loss : %.3f | Top 30 Error loss : %.3f %s"
            % (
                e,
                config["nepochs"],
                loss_error,
                top30_error_rate,
                "[>> BETTER <<]" if updated else "",
            )
        )

        # Update the dashboard
        metrics = {
            "train_CE": train_loss,
            "test_CE": loss_error,
            "top30_error_rate": top30_error_rate,
        }
        if wandb_log is not None:
            wandb_log(metrics)


def test(config):
    raise NotImplementedError


def submit(config):
    submission_config = config["submission"]

    model_directory = (
        f"{submission_config['logdir']}/{submission_config['model_directory']}"
    )
    loaded_config = utils.load_model_config(f"{model_directory}/config.yaml")
    loaded_config["data"]["path"] = submission_config["path"]
    loaded_config["data"]["dataset_kwargs"].update(submission_config["dataset_kwargs"])

    handler_class = models.get_handler_name(loaded_config["model"])

    # Load data
    test_loader, input_size, num_classes = handler_class.get_dataloaders_submission(
        loaded_config["data"]
    )

    # Create model
    logging.info("= Model")
    handler = handler_class(loaded_config["model"], input_size, num_classes)

    # Load pre-trained weights
    handler.load(f"{model_directory}/best_model.pt")

    results = []
    with torch.no_grad():  # Avoid computing gradients during evaluation
        for obs_ids, tests in test_loader:
            obs_ids, tests = obs_ids.to(handler.device), tests.to(handler.device)

            # Forward pass and get top-k predictions
            outputs = handler.model(tests)
            outputs = F.softmax(outputs, dim=1)
            _, predictions = torch.topk(outputs, k=30)

            # Convert results to a list of dictionaries
            results.extend(
                {
                    "Id": obs_id.item(),
                    "Predicted": " ".join(map(str, prediction.tolist())),
                }
                for obs_id, prediction in zip(obs_ids, predictions)
            )
    timestamp = datetime.datetime.now().strftime("%d_%m_%Y_%H_%M_%S")

    # Create a DataFrame and save results to a CSV file
    df = pd.DataFrame(results)
    df.to_csv(f"{model_directory}/submission_{timestamp}.csv", index=False)
    return df


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(message)s")

    if len(sys.argv) != 3:
        logging.error(f"Usage : {sys.argv[0]} config.yaml <train|test|submit>")
        sys.exit(-1)

    logging.info(f"Loading {sys.argv[1]}...")
    command = sys.argv[2]

    if command == "train":
        config = yaml.safe_load(open("config.yaml", "r"))
    else:
        config = yaml.safe_load(open("config_submit.yaml", "r"))

    eval(f"{command}(config)")
