#!/usr/bin/python

import argparse
import os
import subprocess
import sys
import tempfile


def makejob(commit_id, configpath, nruns, og_ds, use_tmpdir):
    return f"""#!/bin/bash

#SBATCH --job-name=group5<3
#SBATCH --nodes=1
#SBATCH --partition=gpu_prod_long
#SBATCH --time=48:00:00
#SBATCH --output=logslurms/slurm-%A_%a.out
#SBATCH --error=logslurms/slurm-%A_%a.err
#SBATCH --array=1-{nruns}

current_dir=`pwd`
export PATH=$PATH:~/.local/bin

echo "Session " ${{SLURM_ARRAY_JOB_ID}}_${{SLURM_ARRAY_TASK_ID}}

echo "Running on " $(hostname)

echo "Copying the source directory and data"
date
mkdir $TMPDIR/code
rsync -r --exclude logs --exclude logslurms --exclude configs . $TMPDIR/code

echo "Checking the data directory"
if [ {use_tmpdir} = 1  ]; then
    if [ ! -d "$TMPDIR/data" ]; then
        echo "Creating and filling the data directory"
        mkdir "$TMPDIR/data"
        cp -r {og_ds}/* "$TMPDIR/data"
    else
        echo "Found data directory"
        ls $TMPDIR/data
    fi
    echo "Updating {configpath} after copying the data"
    sed -i \"s|\[DATAPATH\]|$TMPDIR/data|g\" {configpath}
    cat {configpath}
else
    echo "Updating {configpath}"
    sed -i \"s|\[DATAPATH\]|{og_ds}|g\" {configpath}
fi
echo "Data OK"

echo "Checking out the correct version of the code commit_id {commit_id}"
cd $TMPDIR/code
git checkout {commit_id}

echo "Setting up the virtual environment"
export PATH=/opt/conda/bin:$PATH
source activate ML
pip install -r requirements.txt

# Install the library
python -m pip install .

echo "Training"
python -u -m torchtmpl.main {configpath} train

if [[ $? != 0 ]]; then
    exit -1
fi
"""


def submit_job(job):
    with open("job.sbatch", "w") as fp:
        fp.write(job)
    os.system("sbatch job.sbatch")


# Ensure all the modified files have been staged and commited
# This is to guarantee that the commit id is a reliable certificate
# of the version of the code you want to evaluate
result = int(
    subprocess.run(
        "expr $(git diff --name-only | wc -l) + $(git diff --name-only --cached | wc -l)",
        shell=True,
        stdout=subprocess.PIPE,
    ).stdout.decode()
)
if result > 0:
    print(f"We found {result} modifications either not staged or not commited")
    raise RuntimeError(
        "You must stage and commit every modification before submission "
    )

commit_id = subprocess.check_output(
    "git log --pretty=format:'%H' -n 1", shell=True
).decode()

print(f"I will be using the commit id {commit_id}")

# Ensure the log directory exists
os.system("mkdir -p logslurms")

parser = argparse.ArgumentParser(description="Generates and submits the batch job.")
parser.add_argument("configpath", help="Path to the config.yml file.")
parser.add_argument("nruns", nargs="?", default=1, type=int, help="Number of runs.")
parser.add_argument(
    "--dataset-path",
    default="/mounts/Datasets4/GeoLifeCLEF2022",
    help="Path to the dataset",
)
parser.add_argument(
    "--use-tmpdir",
    action="store_true",
    help="If set, the dataset is going to be copied to the $TMPDIR folder",
)
args = parser.parse_args()

# Copy the config in a temporary config file
os.system("mkdir -p configs")
tmp_configfilepath = tempfile.mkstemp(dir="./configs", suffix="-config.yml")[1]
os.system(f"cp {args.configpath} {tmp_configfilepath}")

# Launch the batch jobs
submit_job(
    makejob(
        commit_id,
        tmp_configfilepath,
        args.nruns,
        args.dataset_path,
        int(args.use_tmpdir),
    )
)
